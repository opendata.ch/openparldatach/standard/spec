# Potentialanalyse Linked Data für POLITMonitor-Daten

**Stand:** Arbeitsversion vom 21.03.2023  
**Autoren:** Jurek Müller, Benedikt Hitz-Gamper  
**Kontext:** Innosuisse Projekt [64902.1 INNO-ICT](https://www.aramis.admin.ch/Beteiligte/?ProjectID=52052)

Die nachfolgende Analyse versucht das Potenzial von RDF/Linked Data zur Nutzung für POLITMonitor-Daten aufzuzeigen.

## Allgemeines Potenzial von Linked Data

Linked Data ist speziell dann von Vorteil, wenn die Daten nicht auf eine bestimmte Anwendung hin optimiert werden sollen, sondern die Daten für eine möglichst breite Nutzung verfügbar sein sollen.

- Linked Data erlaubt die Evolution von Datenschemen, ohne das unterliegende Datenmodell komplett neu entwickeln zu müssen.

### Aussagen aus der Literatur

- “As already discussed, data published in the Web in a reusable form enables new views that have value beyond the sum of the parts and that the original creators might not have anticipated in advance.”  
  [Quelle](https://doi.org/10.1109/MIC.2008.101)
  
- “The lesson here was simple—when a standards-based approach is used, future integration and extended capability is extremely cost effective.”  
  [Quelle](https://doi.org/10.1109/MIS.2012.27)
  
- “RDF represents an extension of the long-established principle of separating content from presentation [...] it will represent an opportunity to free themselves from visual design concerns, concentrate first on publishing relevant high-quality data and let others build the views they want rather than those that someone else assumes they need.”  
  [Quelle](https://doi.org/10.1109/MIC.2008.101)

## Spezifisches Potenzial von Linked Data für POLITMonitor-Daten

- Daten aus dem Politikbetrieb sind grundsätzlich stark vernetzt/verlinkt (keine tabellarischen Daten). Nur im Zusammenspiel der Daten zwischen Politikern, Geschäften, Prozessen und Abstimmungen wird das volle Potenzial der Daten verfügbar.

## Herausforderungen im Zusammenhang mit Linked Data

- Technologisches Wissen rund um Linked Data sowohl auf Seiten der Anbieter als auch auf Seiten der Nutzerinnen ist nicht im gleichen Maß vorhanden wie es für „traditionelle“ Ansätze mit relationalen Datenbankmodellen und REST-APIs ist.
- Linked Data muss mit guten Anleitungen und Tutorials resp. Data-Stories begleitet werden, damit den möglichen Nutzerinnen das Potenzial der Daten aufgezeigt werden kann.

## Alternativen zu RDF/Linked Data

- **Labeled Property Graph (LPG)** (z.B. Neo4j)
  - LPGs bieten von den technologischen Möglichkeiten den gleichen Umfang wie RDF/Linked Data. LPG bieten den Vorteil, dass Aussagen über Aussagen etwas direkter zu formulieren sind (bspw. die Aussage, dass eine bestimmte Anzahl Einwohner für einen bestimmten Zeitpunkt mit einer bestimmten Erhebungsmethode gilt). Mit der Erweiterung von RDF auf RDF\* die momentan in Entwicklung ist, wird dies aber auch für RDF/Linked Data einfacher möglich sein.
  - Nachteilig wirkt sich aus, dass LPGs nicht im gleichen Umfang standardisiert sind wie es RDF/Linked Data mit der tiefen Verankerung in den Webstandards ist. Dies führt zu einer größeren Zukunftssicherheit von RDF/Linked Data im Vergleich zu LPGs.

- **Relationale Datenbank mit REST-API vs RDF/Linked Data**

  - **Vorteile von REST-API Lösungen:**
    - Kosten-Effizienz
    - Know-How auf Anbieter- und Nutzerinnen-Seite vorhanden
  - **Nachteile von REST-API Lösungen:**
    - Auf ein bestimmtes Problem fokussiert
    - Silo-Daten
    - Nicht flexibel im Hinblick auf geänderte Bedürfnisse
    - Keine Semantik in den Daten vorhanden

Im Weiteren werden drei Aspekte näher beleuchtet, die in Linked Data Projekten eine zentrale Rolle spielen:

- das Datenmanagement, welches eine gute Qualität und Zugänglichkeit der Daten sicherstellt
- das Datenmodell und Schema, das eine passgenaue Repräsentation der Daten erlaubt
- das Datenökosystem, in dem der eigene Datensatz eingebettet werden soll

### Datenmanagement

Eine gute Data Governance und ein gutes Datenmanagement sind die Basis für jedes Open Data Vorhaben. Um eine hohe und kontinuierliche Datenqualität gewährleisten zu können, müssen die Rollen von Data Owner und Data Steward klar verteilt sein. Prozesse zur Datensammlung und Aufbereitung müssen kontinuierlich überwacht und gewartet werden. Im kontinuierlichen Dialog mit den Data Usern müssen Probleme und Hürden iterativ identifiziert und gelöst werden.

Durch die intensive Arbeit mit Daten aus verschiedenen Quellen und Erfahrungen mit deren Harmonisierung und Bereitstellung als Produkt hat POLITMonitor bereits die notwendigen Grundlagen, Verantwortlichkeiten und Prozesse, um diesen Herausforderungen zu begegnen. Dabei sollte das Konzept von „Data as a Product“ auch bei der Umstellung von einem bezahlten Service zu einer Open Data Strategie beibehalten bleiben. Das bezieht sich sowohl auf die Qualität und Beschreibung der Daten als auch auf das Engagement mit den Usern. Das gilt besonders für Linked Open Data, welche im Allgemeinen eine größere Einstiegshürde darstellen als konventionelle Datenformate. Eine gute Kommunikation über den Wert der Daten anhand von Show Cases sowie ein aktiver Austausch mit Datennutzenden über Zugänglichkeit, Vollständigkeit und Verständlichkeit der Daten sind daher essenziell.

Obwohl es bei POLITMonitor bereits viele Erfahrungen im Datenmanagement und ein tiefes Verständnis der Daten selbst gibt, fehlt es POLITMonitor bislang an Kompetenzen im Thema RDF/Linked Data. Für die Startphase und den Aufbau der Linked Data Infrastruktur kann externe Unterstützung von Experten aus der Forschung (z.B. Berner Fachhochschule) oder der Privatwirtschaft (z.B. Zazuko) eingeholt werden. Für ein langfristig nachhaltiges Datenmanagement bedarf es jedoch zusätzlich dem Aufbau von internen Kompetenzen. Im Idealfall ist Linked Data kein Nebenprodukt, sondern zentraler Pfeiler der Verteilungsstrategie, sodass der Mehrwert von Linked Data von der gesamten Organisation verstanden und genutzt werden kann.

### Datenmodell und Schema

Das Datenmodell für POLITMonitor hat gewissen Anforderungen zu erfüllen. Parlamentsdaten sind oft mit einer Historie verbunden (Amtszeiten, Legislaturperioden, Verlauf von Geschäften etc.). Diese Historie sollte im Datenmodell abgebildet sein, sodass z.B. Informationen über die Zusammensetzung des Parlaments oder einer Kommission zu einem bestimmten Zeitpunkt oder die Historie eines Geschäfts intuitiv abrufbar sind. Eine weitere Herausforderung an das Datenmodell ist die gleichzeitige Abbildung von Daten aus Nationalrat, Ständerat und 26 Kantonsparlamenten. Das Datenmodell sollte also flexibel genug sein, um verschiedene Gesetzgebungsverfahren und Parlamente (inkl. bspw. Landsgemeinden) abbilden zu können.

Für eine optimale Wiederverwendbarkeit beim Aufbau eines Linked Data Models (bezw. Schemas/Ontologie) ist es von Vorteil, sich so weit wie möglich auf bestehende und etablierte Schemas zu stützen. Gleichzeitig hat jeder Datensatz einen spezifischen Kontext und damit Anforderungen an die Metadaten und das Datenmodell. Bestehende Lösungen können dafür häufig für die eigenen Anforderungen erweitert und angepasst werden.

In Bezug auf Parlamentsdaten (Geschäfte, Abstimmungen, Mitglieder etc.) gibt es allerdings bisher keine national oder international etablierten Linked Data Standards. Zusammen mit der Einzigartigkeit des Schweizer politischen Systems sowie der Vielfalt der politischen Verfahren in den 26 Kantonen ist im Fall der Veröffentlichung der Daten als Linked Data der Aufbau eines eigenen Datenmodells/Schemas unausweichlich. Es gibt jedoch eine Reihe von Projekten, die als Grundlage und/oder Inspiration dienen und damit den Prozess vereinfachen können:

- **DemocraSci**  
  [Projektwebsite 1](https://brandenbergerlaurence.com/democrasci/)  
  [Projektwebsite 2](https://datascience.ch/project/democrasci/)  
  Ein Projekt aus der Wissenschaft (ETH Zürich & SDSC), das mithilfe von digitalisierten Dokumenten einen Knowledge Graph des Schweizer Parlaments (Bund) mit Reden, Geschäften, Parlamentarier:innen, Abstimmungen uvm. der letzten 130 Jahre erstellt, inklusive NLP-basierter Themenzuteilung. Die Daten sind in einer LPG-Datenbank (Neo4j) abgelegt. Die Daten und das Schema sind noch nicht veröffentlicht, was aber in Zukunft passieren soll. Das zugrunde liegende Datenmodell könnte eine gute Grundlage für POLITMonitor darstellen, auch wenn die Umwandlung zu RDF und die Berücksichtigung der Spezifika der kantonalen Ebenen mit Sicherheit größere Anpassungen erfordern. Die Arbeiten von POLITMonitor und DemocraSci scheinen sehr ähnlich zu sein, was so oder so eine gute Absprache erforderlich macht.

- **ParliamentSampo**  
  [Projektwebsite](https://seco.cs.aalto.fi/projects/semparl/en/)  
  Die Reden aus dem finnischen Parlament als Linked Open Data, inklusive Mitglieder, Organisationen etc. Ein vorbildliches und ausgereiftes Linked Data Projekt. Die Einträge sind sehr umfangreich mit vielen Links nach außen (z.B. Bilder der Mitglieder von Wikimedia Commons; `sameAs`). Das Schema ist auf Englisch und kann als Inspiration dienen (allerdings ist der Kontext von Reden und Geschäften ein etwas anderer). Vom Schema ist nur ein kleiner Teil beschrieben. Der Rest kann aus den Daten selbst erschlossen werden.  
  Beispiel-URI: <http://ldf.fi/semparl/people/p170>  
  Schema/Vocabulary (Nur ein kleiner Teil): <http://ldf.fi/schema/semparl/>  
  SPARQL-Endpoint: <https://ldf.fi/semparl/sparql>

- **LinkedEP**  
  [Projektwebsite](http://linkedpolitics.ops.few.vu.nl/web/html/home.html)  
  [Datamodel](http://linkedpolitics.ops.few.vu.nl/web/html/datamodel.html)  
  Die Debatten aus dem Europaparlament als Linked Data, inklusive Mitglieder, Organisation, Rolle etc. Das Datenschema ist auf Englisch und referenzierbar. Es könnte als Ausgangspunkt oder Inspiration dienen, auch wenn der Kontext von Reden ein etwas anderer ist.  
  Literatur: <https://doi.org/10.3233/SW-160227>

- **Ontologie des italienischen Parlaments (OCD)**  
  [Referenzdokument](https://dati.camera.it/ocd/reference_document/#classes)  
  Ein Schema des italienischen Parlaments inklusive Abgeordneten, Parteien, Ausschüsse etc. Das Schema ist auf Italienisch und daher nur bedingt geeignet für direkte Verlinkungen. Allerdings kann es als Inspiration für ein Schweizer Datenmodell dienen.

- **OParl**  
  [Projektwebsite](https://dev.oparl.org)  
  Oparl ist ein Europäischer REST-API Standard für Parlamentsdaten von der Open Knowledge Foundation. Der Standard wurde 2016 veröffentlicht und 2018 das letzte Mal aktualisiert.  
  Es handelt sich nicht um ein Linked Data Schema, allerdings könnte von der Struktur ein solches abgeleitet werden. Es gibt bereits einen Linked Data Fork von OParl namens OpenGovLD (<https://github.com/OpenGovLD/specs/wiki>), welcher aber 2014 das letzte Mal aktualisiert wurde, also noch vor der Veröffentlichung von OParl 1.0.

- **European Legislation Identifier (ELI)**  
  [Projektwebsite](https://op.europa.eu/en/web/eu-vocabularies/dataset/-/resource?uri=http://publications.europa.eu/resource/dataset/eli)  
  ELI ist einerseits ein System zur Benennung von URIs für Gesetzestexte und andererseits ein Schema für Metadaten zu Gesetzestexten. Beides könnte in begrenztem Maße auch für die Texte der Geschäfte relevant sein. Etwaige Überlappungen müssten im Detail geprüft werden.

- **XML-Schemata**  
  Für Gesetzestexte (Akoma Ntoso <http://www.akomantoso.org/>) und parlamentarische Debatten/Vorstösse (ParlaMint <https://github.com/clarin-eric/ParlaMint> / Specification of Parla-CLARIN <https://clarin-eric.github.io/parla-clarin/>) gibt es verschiedene XML-Standards, die vor allem aus den Sozialwissenschaften stammen und Textanalysen ermöglichen sollen. Interessant wäre allenfalls, die Schemata auf die Codierung von Metadaten hin zu Untersuchen und als Inspirationsquelle zu nutzen. Es gibt Beispiele von der Umwandlung von Daten von Akoma Ntoso zu RDF (Barabucci et al. 2009).

- **version.link**  
  [Projektwebsite](https://version.link/)  
  Ein sich von BFH und Bundesarchiv im Aufbau befindliches RDF-Schema zur Abbildung von versionierten RDF-Daten. Könnte als Grundlage dienen, um verschiedene Versionen bzw. die zeitliche Historie von Geschäften, Mitgliedern, Organisationen etc. abzubilden.

Auch wenn die beschriebenen Schemas und Projekte eine solide Grundlage für das Entwickeln eines auf den Kontext angepassten RDF-Datenmodells bieten, ist die Entwicklung mit einem Initialaufwand verbunden. Gleichzeitig bietet die Abwesenheit von RDF-Standards für Parlamentsdaten auch die Möglichkeit, hier eine Vorreiterrolle einzunehmen und national und eventuell auch international Einfluss auf die Schema-Landschaft zu nehmen und somit durch Wiederverwendung des Schemas und der Daten auch zukünftige Linked Data Projekte rund um Parlamentsdaten (z.B. von Kantonen oder Gemeinden) zu erleichtern.

### Datenökosystem

In der Schweiz befindet sich mit LINDAS ein ganzes Ökosystem rund um Linked Data im Verwaltungsumfeld im Aufbau, für das es ein klares Commitment von verschiedenen Seiten gibt. Die mittel- und langfristige Zukunft ist also sichergestellt. Dank der Möglichkeiten von Linked Data ließen sich POLITMonitor Daten in RDF/Linked Data in dieses Ökosystem einbinden.

Im Rahmen der Öffnung von POLITMonitor sollten unbedingt Abklärungen getroffen werden, wie sich POLITMonitor positioniert im Vergleich zu Projekten, die ähnliche Absichten haben (DemocraSci) oder zumindest klare Überschneidungen (z.B. Lobbywatch) aufweisen.

## Weitere Literatur

**Finnische Linked Data Projekte**

- Barabucci, Gioele, & Cervone, Luca, & Palmirani, Monica, & Peroni, Silvio, & Vitali, Fabio. (2009). Multi-layer Markup and Ontological Structures in Akoma Ntoso. 6237. 133-149. [DOI: 10.1007/978-3-642-16524-5_9](https://doi.org/10.1007/978-3-642-16524-5_9).

- Sinikallio, L., Drobac, S., Tamper, M., Leal, R., Koho, M., Tuominen, J., ... & Hyvönen, E. (2021, August). Plenary debates of the Parliament of Finland as Linked Open Data and in Parla-CLARIN markup. In 3rd Conference on Language Data and Knowledge (LDK 2021). Schloss Dagstuhl-Leibniz-Zentrum fur Informatik GmbH, Dagstuhl Publishing.

- Leskinen, Petri, Eero Hyvönen, and Jouni Tuominen. "Members of Parliament in Finland knowledge graph and its linked open data service." Proceedings of SEMANTiCS–In the Era of Knowledge Graphs, Amsterdam (2021): 255-269.

- Hyvönen, Eero, et al. "ParliamentSampo: Infrastructure for Publishing the Plenary Speeches and Networks of Politicians of the Parliament of Finland as Open Data Services." (2023).

- Poikkimäki, Henna, et al. "Analyses of Networks of Politicians Based on Linked Data: Case ParliamentSampo–Parliament of Finland on the Semantic Web." New Trends in Database and Information Systems: ADBIS 2022 Short Papers, Doctoral Consortium and Workshops: DOING, K-GALS, MADEISD, MegaData, SWODCH Turin, Italy, September 5–8, 2022, Proceedings. Cham: Springer International Publishing, 2022.
